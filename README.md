Here is a **Jupyter Book** example with the basic commands to create a Jupyter Book and write with Markdown in the notebooks.

Follow the [link](https://lauracagigal.gitlab.io/jupyterbook_example/) below to open the book:

https://lauracagigal.gitlab.io/jupyterbook_example/

