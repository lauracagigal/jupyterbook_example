#!/usr/bin/env python
# coding: utf-8

# # Markdown for basic jupyter notebooks

# Most of the common markdown funcionalities can be found [here](https://jupyterbook.org/reference/cheatsheet.html#line-comment).
# <br> This is just a summary of the most common ones

# ## Write

# Regular text

# *Italic text*

# **Bold text**

# ***Bold and italic text***

# ## Insert features

# ### Insert hyperlink

# [Gitlab folder](https://gitlab.com/geoocean)

# ### Insert image

# ```{figure} images/awt0.jpg
# ---
# name: enso
# ---
# El Niño Southern Oscillation.
# ```
# 

# There are different ways of referring to the figure, using the **name** assigned. Here some examples

# This {ref}`figure <enso>`
# is an example.

# {numref}`Figure %s <enso>`
# is an example.

# ### Insert a list

# - List 0
# - List 1
# - List 2

# ### Equations

# Equation example
# 
# $$
# z=\sqrt{x^2+y^2}
# $$

# Equation example in line:  $z=\sqrt{x^2+y^2}$

# ## Special text

# ```{note}
# Here is a note!
# ```

# ```{warning}
# Warning example
# ```

# ```{hint}
# Hint example...
# ```

# There is also the possibility to hide the info and add a + buttom

# :::{admonition} Click here!
# :class: tip, dropdown
# **Hidden list**
# - list1
# - list2
# -list 3
# :::

# In[ ]:




