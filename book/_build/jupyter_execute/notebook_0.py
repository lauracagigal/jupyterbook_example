#!/usr/bin/env python
# coding: utf-8

# # Install and create a jupyter book

# **These are the steps that need to be followed to create the book:**

# ## Install and configure config and toc file

# The first step is to install jupyter-book:
# 
# - ***pip install jupyter-book***
# 
# Then, to create the book you have to run: ***jupyter-book create book_name***. This will create different files, and these are the ones we have to modify:
# 
# - `_toc.yml`. This has the index of the book, and should look similar to:
# 
# 
# ```{figure} images/toc_format.png
# ---
# name: toc
# width: 400px
# height: 250px
# ---
# Format of toc file.
# ```
# 
# 
# - `_config.yml`
# 
# ```{figure} images/config_format.png
# ---
# name: config
# width: 600px
# height: 400px
# align: center
# ---
# Format of config file
# ````
# 

# ```{caution}
# It is important to turn the execute notebooks line to 'off' in the config file
# ```

# ## Build the book

# Inside the book folder, run: ***jb build ./***
# This will create a html folder with the index of the book

# ## Create pdf file

# Inside the book folder, run: ***jb build book_example/ --builder pdflatex***
# This will create a latex folder with the book.pdf inside

# This will create a pdf file based on latex with a table of contents. 
