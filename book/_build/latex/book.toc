\selectlanguage *{english}
\contentsline {chapter}{\numberline {1}Install and create a jupyter book}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Install and configure config and toc file}{3}{section.1.1}%
\contentsline {section}{\numberline {1.2}Build the book}{5}{section.1.2}%
\contentsline {section}{\numberline {1.3}Create pdf file}{5}{section.1.3}%
\contentsline {chapter}{\numberline {2}Markdown for basic jupyter notebooks}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}Write}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Insert features}{7}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Insert hyperlink}{7}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Insert image}{7}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Insert a list}{7}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Equations}{8}{subsection.2.2.4}%
\contentsline {section}{\numberline {2.3}Special text}{8}{section.2.3}%
